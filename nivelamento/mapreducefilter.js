const CADASTRO = {
    descricao: "cadastro de pessoas",
    pessoas: [
        {
            nome: "joão da silva",
            idade: 25,
            profissao: "médico",
        },
        {
            nome: "josé da silva",
            idade: 21,
            profissao: "engenheiro",
        },
        {
            nome: "maria da silva",
            idade: 20,
            profissao: "estudante",
        }
    ]
}

const pessoas = CADASTRO.pessoas;

// mapeamento
pessoas.map((pessoa) => {
    console.log("nome da pessoa: " + pessoa.nome);
});

// filtragem
maioresQueVinte = pessoas.filter((pessoa) => pessoa.idade > 20);
maioresQueVinte.map((pessoa) => {
    console.log("nome da pessoa maior que vinte: " + pessoa.nome);
})

// reducao 
const somaIdades = pessoas.reduce((idadeAcumulada, pessoa) => {
    return idadeAcumulada + pessoa.idade
}, 0);
console.log("soma das idade: " + somaIdades);